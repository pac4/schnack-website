---
title: Showcase
_template: templates/_markdown_page.html
nav_order: 4
---

# Showcase

Schnack is designed to work hand in hand with the ALPACA engine ("<u>A</u> <u>L</u>ibrary for <u>P</u>oint <u>A</u>nd <u>C</u>lick <u>A</u>dventures"),
which is also free open source software and can be found on [GitHub](https://github.com/pinguin999/ALPACA).

The dialog player can be tested live in the browser using *Schnacker.js*.
The live demo can be found <a href="{{base_url}}/interpreter" target="_blank">here</a>.


Further, Schnack has been used in the following games:

## 2080 - An Odyssey in Space
A short Demo for showing the capabilities of the ALPACA engine:

![Screenshot of the 2080 Demo: A female astro-engineer stands in a starship quarter]({{base_url}}/img/screenshot-2080.png)

[Online-playable Demo](https://hobbyspieleentwicklerpodcast.de/2080-Demo)



## Cure for the Woods
A point and click adventure game, created during the Global Game Jam 2023:

![Screenshot of the Global Game Jam Game 'Cure for the Woods': A blue forest spirit stands in a wooden kitchen where a mushroom chef cooks resin]({{base_url}}/img/screenshot-cftw.png)

[Online-playable Demo on Itch.io](https://brain-connected.itch.io/cure-for-the-woods)