---
title: Blog System
blog_date: 2023-01-03
blog_author: Carste Pfeffer
blog_summary: Today I write about the blogging capabilities of Rattlesnake
tags:
    - misc
    - tech
_template: templates/_blogpost.html
---

This first blog post is meant to demonstrate that *Rattlesnake* is
capable to realize simple blog systems. There is nothing special
needed in *Rattlesnake* for blog posts, though. It only uses the
*Jinja* templating features, combined with the *Markdown* parsing and
front-matter metadata, which is also available for normal sites built
with *Rattlesnake*.

This post for example has the following front-matter:

    ---
    title: Blog System
    blog_date: 2023-01-03
    blog_author: Carste Pfeffer
    tags:
        - misc
        - tech
    ---