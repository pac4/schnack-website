---
title: Second Post
blog_date: 2023-01-04
blog_author: Carste Pfeffer
blog_summary: This is my second post here...
tags:
    - misc
_template: templates/_blogpost.html
---

Actually, I only wrote this one in order to show how it looks with more than one blog post...