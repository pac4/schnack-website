---
title: 05 - Voice Acting Support
userguide_author: Carsten Pfeffer
userguide_summary: This article shows how Schnack supports the integration of voice actors into the development process
_template: templates/_user-guide-article.html
---

In the last tutorial, we worked on the localization of our dialogs.
This tutorial will show you what *Schnack* does to help you adding
voice acting to your game.

## PDF Export
Schnack offers you the possibility to export all existing lines to
a PDF file which you can then hand over to your voice actors.
The export dialog has basically two settings:
First, you can select which locale should be exported. One PDF file can
only contain one locale. By default, the name of the locale will be suffixed
to the file name of the PDF.
Second, you can select, which voice lines should be exported. You can eihter
export all of the characters or only a selection in order to give the voice
actors only the lines for their characters:

![The PDF export dialog]({{base_url}}/img/user-guide/05-export-pdf.png)

The exported PDF file (in English with all characters exported) looks like this:

![The exported PDF file]({{base_url}}/img/user-guide/05-the-pdf.png)

The document is printed in a monospace font in order to provide a clear,
non-distracting formatting. Further, each take is prefixed with the node ID.
This way, you can instuct your voice actors to provide the takes as sound files
named after the node IDs (e.g., `012.ogg` will contain the take
*"There are so many of them! It's a nightmare!"*). This way, you can automatically
play back the according audio file in your engine, when the node with the given ID
is executed (This is also a reason why node IDs are unique across all dialogs).

The *Schnacker* runtimes don't provide any audio playback. However, if you are able
to play sound in your engine, it should be pretty straight-forward to implement this
when using the *Schnacker* runtime in your game.
