---
title: 01 - Overview
userguide_author: Carsten Pfeffer
userguide_summary: "The basics, file format and runtimes"
_template: templates/_user-guide-article.html
---

Schnack allows you to manage dialog text snippets and logic using a node-based editor and the Lua scripting language ([www.lua.org](https://www.lua.org)). This means that characters and their properties (local variables), as well as global variables and their default values can be managed.

## The Schnack File Format

While the *Schnack* editor is used to edit dialogs and create *Schnack* files, these files must then somehow be integrated into the game.
This is where the *Schnacker* libraries come into play (pun intended). There are *Schnacker* libraries for a couple of programming languages.
You can use them in your game in order to play the dialogs. The *Schnack* files are based on the JSON standard and the particular format
is described in the manual, so it shouldn't be too hard to create your own implementation if there is no version for the language you are using
(especially since the source code of the existing libraries is available).

The default file ending for the dialog files is `.schnack` and not `.json` (although this also works perfectly fine if you want to do it that way).
However, the idea behind this decision was that it would be easier to automatically open `.schnack` files with the dialog editor, rather than
with your favorite text editor (especially on Windows).

One *Schnack* file contains:

- a list of characters
- a list of dialogs
- variables used for game / dialog logic
- localization information (optional)

This means that everything is contained within a single file.

## The following tutorials
The following articles will describe, how you create new characters, dialogs and how you add some logic to these dialogs.
In the later tutorials, we will cover advanced topics like localization and the actual integration into a game.