---
title: 07 - Integrating Schnacker.js Into Your Game
userguide_author: Carsten Pfeffer
userguide_summary: This tutorial shows you how you can integrate the Schnacker Javascript runtime into your game in order to play dialogs
_template: templates/_user-guide-article.html
---

The second-most important *Schnack* runtime is *Schnacker.js*. It has the same functionality and a very similar API
to *Schnacker* (the only differences are due to the particular traits of both programming languages -- method overloading, etc.).
The Javascript implementation of *Schnacker* uses the *Fengari* library for providing *Lua* scripting support.
The source code of the Schnacker.js library can be found
in the [Gitlab Repository](https://gitlab.com/pac4/schnacker.js/). The source
code repository also contains an example implementation in the `dist/index.html` file.
This is the same as the [online dialog player]({{base_url}}/interpreter). You can also have a look at the source code
of that page in order to get an idea how to use *Schnacker.js*.

## Using the JS File

There is currently no NPM package for *Schnacker.js*, but you can include the compiled version in your game,
website, or wherever you want to use it:

    <script src="schnacker.js"></script>

## Integrating Schnacker.js Into Your Code

The first thing you are probably going to do is loading a *Schnack* file. In contrast to
the C++ library, there is only one `loadFromString` method (this is due to the fact that
*Javascript* does not support method overloading). The method expects a *Lua* state
(from the *Fengari* library, which is included in *Schnacker.js*), the string contents
of the file and a boolean that indicates whether the variables should be initialized
according to their default values in the file:

    let schnackFile = schnacker.SchnackFile.loadFromString(null, schnackFileContents, true);

If the *Lua* state is *null*, the runtime will create one. If you use *Lua* also for other
things than the dialog engine, you might want to just give *Schnacker.js* your instance via
this parameter. If you let *Schnacker.js* create this instance, you can access it later by
the `luaState` field of the `SchnackFile` class.

The last parameter (`initializeVariables`)  gives you the possibility to specify whether you
want the variables to be initialized with their default values. Depending on your
savegame implementation, it might be convenient to deactivate the initialization of
default values. However, if you don't understand the reason, just set this parameter to
`true`, since this is what you most probably want.

There is a second method, similar to `loadFromString`, which is called `loadFromObject`
and has the same arguments, with the difference that the second one is not expected to be
a string, but an object (if you already have the data deserialized, there's no need to
JSONify it, just to parse it again in the next step).

[This tutorial is not yet finished...]

<!-- 
If your gamne supports multiple locales, you then might want to set the current locale of the
dialog engine as a next step (the parameter expects an instance of `std::string`):

    schnackFile->setCurrentLocale(game->getCurrentLocale());

## Playing Dialogs

The `SchnackFile` class has a field called `dialogs`, which is a map from `std::string` to
`std::shared_ptr<Dialog>`. This means that you can access dialogs by their name (the name
you chose in the tree view of the dialog editor):

    auto currentDialog = schnackFile->dialogs[dialogName];

When you want to start the playback of the particular dialog, you first have to get its entry node:

    auto currentNode = currentDialog->getEntryNode();

The `Node` class has a `step(dialog)` method, which can be called in order to evaluate logic of the
node. It will automatically check preconditions of the node's successors and even execute the
successors, if they are anything but *text nodes* or *question nodes*. This is due to the fact
that text nodes and question nodes require your assistance to be executed properly.

Whenever the execution of a chain of nodes reaches a text node, it will return a `TextStepResult`,
which contains the localized text that should be displayed to the user, the ID of the node that
contains this text, a reference to the character who says this text and a reference to the new
*current node*. In this case, you should probably display the `text` and the name of the `character`
on screen (since *Schnacker* doesn't know your engine, you have to do this). If you have voice acting
and followed the proposed naming convention for speech audio files, you can also play the audio
file with the ID of the node (again, you have to do this, because *Schnacker* is engine-agnostic).

In order to continue the dialog (after the audio playback is finished, or the player clicked *"next"*,
or however your game solves this), you can use the `currentNode` field of the `TextStepResult` and
call its `step(dialog)` method, which will then again execute all nodes from the current position until
it reaches a text node or a question node.

When a question node is reached, an `AnswersStepResult` is returned. In this case you should prompt
the player with the possible answers. These can be found in the `answers` field of the class.
It is a list (or rather `std::vector`) of tuples, containing a node ID and a localized string.
You can show the strings to the player. When the player chooses one of the options, you can call
the `chooseAnswer(dialog, nodeId)` method of the `AnswersStepResult`. It will then return a new
current node, which then again has a `step(dialog)` method to be called.

If the result is a null pointer or if the result is a `TextStepResult` containing a null pointer
for `currentNode`, the dialog's execution is done.


In simple C++ pseudo-code this would look approximately like this:

    // load the dialog
    auto currentDialog = schnackFile->dialogs[dialogName];
    // get the entry point
    auto currentNode = currentDialog->getEntryNode();

    // run until the dialog is done
    while(currentNode != nullptr)
    {
        // run the current node
        auto result = currentNode.step(currentDialog);

        // if there is no further node to execute, the dialog is finished
        if(result == nullptr)
            return;

        // the new current node is taken from the step result
        currentNode = result->currentNode;

        // if the result is a text result, show the text and play the voice
        auto textResult = std::dynamic_pointer_cast<TextStepResult>(result);
        if(textResult)
        {
            showText(textResult.character, textResult.text);
            playVoiceSample(textResult.nodeId);
        }
        else
        {
            // if the result is a question to the player, provide the possible answers
            // and wait for a choice. The new current node will depend on that choice
            auto answersResult = std::dynamic_pointer_cast<AnswersStepResult>(result);
            if(answersResult)
            {
                NodeId answerId = promptPlayerForAnswer(answersResult.answers);
                currentNode = answersResult->chooseAnswer(currentDialog, answerId);
            }
        }
    }

In a real-time game, this loop might not be fully sufficient, since you can not
stop the execution of the whole game until the player made a choice. However,
this is the most compact version that helps to understand how the library is intended
to be used. -->

