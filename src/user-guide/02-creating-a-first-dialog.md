---
title: 02 - Creating a First Dialog
userguide_author: Carsten Pfeffer
userguide_summary: Creating a character and some spoken text
_template: templates/_user-guide-article.html
---
If you open *Schnack* for the first time, you will most probably see something that looks like the following picture:

![The Schnack splash screen shows a list of recent files and buttons for opening other files and creating new ones]({{base_url}}/img/user-guide/02-spashscreen.png)

The splash screen usually list a couple of recent files in order to make them easily accessible (yes, this idea was stolen from *Blender 3D*).
Further, there is a button for opening a file using your operating system's file dialog and also a button for creating a new file.
If you open *Schnack* for the first time, the list of recent files should be empty.

Press *"Create New"* to create a new file.

## Creating a New Character
An empty editor window will open and it is likely that it will look like this:

![The empty editor]({{base_url}}/img/user-guide/02-empty-editor.png)

Before actually writing a dialog, we need to add a character. Our first dialog will be very simple,
so we will need only one character. Let's say we write the dialog (or rather monologue) our hero or heroine
speaks when she inspects a particular item (one could say that this is also a dialog - a dialog with the player).

In order to add a new Character, press *"Add"* at the bottom right of the screen in the *"Characters Panel"*
(down below to the real bottom, not the *"Add"* button in the *"Properties"* field).
You will be asked for a *canonical* character name:

![Input dialog for the canonical character name]({{base_url}}/img/user-guide/02-canonical-character-name.png)

The *canonical* character name is the unique name which identifies the character.
It has to be a valid Lua identifier, since it will be used to access the character's properties
when scripting logic. This name does not necessarily have to be the same as the *display character name*.

For this tutorial, enter something like `player` into the prompt and hit *"OK"*.
The new character will be shown in the list of characters:

![The characters panel with the first character]({{base_url}}/img/user-guide/02-first-character.png)

The *"P"* in the green circle is an auto-generated avatar that is meant to make it easier for you to
identify which character says what. If you add other characters, they will get other colors.
You will notice that next to the avatar, there is the text *"player"* twice. The upper text is greyed
out a little bit. This is the canonical name. The lower text is written in quotes. This is the display
name (i.e., how the name should be displayed in the game). Double-click the quoted text in order to change
the character's display name. In this example, we will call our heroine *"Minerva the Sorceress"*:

![Rename character prompt]({{base_url}}/img/user-guide/02-rename-character.png)

You will see that the avatar image changed from *"P"* to *"M"* and that also the color changed.
The letter and color of the circle are depending on the display name of the character.
Now that we have a character, we are ready to create our first dialog.

You will notice that there is also a list of properties in the character panel.
It allows you to define properties of a character which can then be used when adding
logic to your dialogs. However, we will skip this for our first dialog, since this one won't require
character properties.

## Creating a Simple Dialog

As already stated in the introduction, one *Schnack* file can contain multiple dialogs.
The dialogs can be managed in the *"Dialogs"* panel (or *"Dialog Tree"*). The dialog panel
is a tree view that allows you to add dialogs in a hierarchial manner, similar to how you
would do this in the file system. The root of the tree is the name of your game.
By default, it is just called "Dialogs". You can either double-click the root element or
right-click it and choose *"Rename Project"* in the context menu. I'll call it *"Minerva's
Great Adventure"*.

![Rename project]({{base_url}}/img/user-guide/02-rename-project.png)

In the context menu, you can not only rename the project, but you also can add dialogs or folders.
Folders are meant to organize dialogs in large projects (again, just like you would do in the file system).
However, for this small project, we won't need any folders and can create our first dialog direcly
under the root node.

Let's say our heroine inspects a book and says something as a hint to the player.
In order to create the dialog (or monologue, respectively), right-click the root
node and select *"Add Dialog"*. A new dialog will be added to the tree and you can
enter the name of the dialog. Let's name that dialog `book`. Confirm the name with
the enter key and click on the newly generated dialog:

![New dialog]({{base_url}}/img/user-guide/02-new-dialog.png)

After selecting the dialog in the tree, you can start actually editing the dialog in the node editor,
which should have the cation *"Dialog 'book'"*. Add a text node by right-clicking the empty background and
select *"Add Node"* &rarr; *"Add Text Node"*.

![Adding a text node]({{base_url}}/img/user-guide/02-add-text-node.png)

A new node will appear on the canvas. Double-click the dark text field in the middle of the node
and enter the text you character should say when this dialog is played. For this example, 
let her say *"The book cover looks like a four-year old drew this."*. Then click *"OK"* or
CTRL+ENTER. Your text node should now look similar to this:

![The first text node]({{base_url}}/img/user-guide/02-first-text-node.png)

The node contains the information who says this (the character *"Minerva the Sorceress"*).
Further it contains the *"Only Once"* flag, a precondition field and a script field.
However, for this example, we will omit these and concentrate on the text in the center of the node.
This is what will actially been said by the character. The red border of the node indicates that
this is the *"entry node"* of the dialog. This node will always be executed first, when this
dialog is played. This might not be relevant for a dialog with only one node, but it will be relevant
in the more-advanced examples.

This is our first dialog. Unspectacular? Yes, but it is the first building-block for more-advanced
conversations. Press CTRL+S (or select *"File"* &rarr; *"Save"*) in order to save the file.
A file dialog will ask you where to save the file. Select a location of your choice and give the
file a meaningful name (e.g. `minerva.schnack`). If no file extension is provided, the editor will
automatically add `.schnack`.

## Test Your First Dialog

You can use the <a href="{{base_url}}/interpreter" target="_blank">online-runtime</a>. of *Schnack*
to test your dialog file. Follow the link and drag your newly created file onto the website.
In the list to the left, there should be only one dialog called `book`. press the green button in the
middle to execute it and you will see something like the following:

![Testing the first dialog]({{base_url}}/img/user-guide/02-testing-the-first-dialog.png)

Well, that's not quite overwhelming, since it only shows a balloon with your text.
However, it gives you the possibility to test your dialogs in a very simple way.
Since this is rather boring, let's go back to the editor and add another text node.
Then connect the output of the first node with the input of the second node.
The inputs and outputs are the small circles on the left and right sides of the nodes.
Then enter a text into the second node by double-clicking the dark text content panel.
Your dialog should now look like this:

![Adding a second text node]({{base_url}}/img/user-guide/02-two-text-nodes.png)

Save the file and text it in the browser one more time (you need to drag-and-drop it again onto the test website).
There should still be only one dialog, but when you execute it now, there should be two balloons that appear one
after another:

![Testing the second text node]({{base_url}}/img/user-guide/02-testing-two-text-nodes.png)

The result is still not very spectacular, but you get the idea. Everything that is said by a character is
located inside a text node (all other node types are mainly for logic). Every text node results in a balloon
(or rather a dialog snippet, depending on how you visually implement this in your game).

Congratulations! You just created your first simple dialog using *Schnack* and now you are ready to
continue with some more advanced stuff.
