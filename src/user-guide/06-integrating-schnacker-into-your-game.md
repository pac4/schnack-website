---
title: 06 - Integrating Schnacker Into Your Game
userguide_author: Carsten Pfeffer
userguide_summary: This tutorial shows you how you can integrate the Schnacker C++ runtime into your game in order to play dialogs
_template: templates/_user-guide-article.html
---

Schnacker is the primary runtime implementation for Schnack dialog files.
It is written in and for C++ and it is used in the ALPACA point and click
adventure engine. Further, it uses the sol2 Lua bindings for providing Lua
scripting support. The source code of the Schnacker library can be found
in the [Gitlab Repository](https://gitlab.com/pac4/schnacker/). The source
code repository also contains an example implementation in the `main.cpp` file.

## Adding a Git Submodule

If you use CMake, the simplest way to integrate *Schnacker* into your C++ game
is to use it as a git submodule. Open a terminal and navigate to your project. 

Then create a folder for the referenced libraries (e.g. `subprojects`) and add a git submodule
to your repository:

    git submodule add https://gitlab.com/pac4/schnacker.git subprojects/schnacker

The *Schnacker* library will then be cloned into the directory `subprojects/schnacker`.

## Configuring your CMakeLists

Then add the following lines to your CMakeLists.txt`:


    add_subdirectory(subprojects/schnacker/)

<!-- -->

    target_link_libraries(<<your-project-name>> PRIVATE
                          schnacker
    )

<!-- -->

    target_include_directories(<<your-project-name>> PRIVATE ${CMAKE_BINARY_DIR}/include
                               subprojects/schnacker/src)

The first one will add the directory of the *schnacker* project to the build.
This is the path where the `CMakeLists.txt` and source files of *Schnacker* are located.
The second block specifies that schnacker should be considered as a library by the linker.
The third block adds the source directory of *Schnacker* to the include directories, so that
your include statements will find the *Schnacker* headers. Depending on what you already have
in your *CMakeLists* file, you might be required to slightly modify the above lines.

If you would like to have interprocedural optimization for release builds, also add the
following lines to your *CMakeLists* file:

    include(CheckIPOSupported)
    check_ipo_supported(RESULT lto_supported OUTPUT error)

<!-- -->

    if(CMAKE_BUILD_TYPE STREQUAL "Release")
        if(lto_supported)
            set(CMAKE_INTERPROCEDURAL_OPTIMIZATION_RELEASE TRUE)
            set_property(TARGET schnacker PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
        endif()
    endif()

The first two lines will import the mechanism that checks whether the compiler supports
interprocedural optimization. The check result is stored in the variable `lto_supported`.
The second block activates the optimization for *Schnacker* when doing a release build.

## Integrating Schnacker Into Your Code

In order to use schnacker in your game code, import the schnacker header:

    #include <schnacker.hpp>

The first thing you are probably going to do is loading a *Schnack* file. There are three
overloads for the `loadFromString` method (loading from file is not integrated in the library,
because the chances are high that your engine or target platform will access the file system
in a way *Schnacker* doesn't know):

    static std::shared_ptr<SchnackFile> loadFromString(std::string contents);

    static std::shared_ptr<SchnackFile> loadFromString(std::string contents,
                                                       bool initializeVariables);

    static std::shared_ptr<SchnackFile> loadFromString(std::shared_ptr<sol::state> luaState,
                                                       std::string contents,
                                                       bool initializeVariables);

The first overload just parses the parameter `contents` as a JSON string, creates a new
*Lua* state and initializes all variables according to their default values in the file.

The second overload does the same, but gives you the possibility to specify whether you
want the variables to be initialized with their default values. Depending on your
savegame implementation, it might be convenient to deactivate the initialization of
default values. However, if you don't understand the reason, just set this parameter to
`true`, since this is what you most probably want.

The third overload does the same as the second one, but additionally allows you to provide
a particular *Lua* state to be used by *Schnacker*. This *Lua* state is expected to be an
instance from the *sol* C++ wrapper library for *Lua*, which is referenced by *Schnacker*.
If you use *Lua* also for other things than the dialog engine, you might want to just give
*Schnacker* your instance via this parameter, or you can let *Schnacker* create this instance
(e.g., by using one of the overloads above) and then access it by the `luaState` field of the
`SchnackFile` class.

An example call of this method could look like this:

    auto schnackFile = schnacker::SchnackFile::loadFromString(luaState, engine::readAsset(fileName), true);

If your gamne supports multiple locales, you then might want to set the current locale of the
dialog engine as a next step (the parameter expects an instance of `std::string`):

    schnackFile->setCurrentLocale(game->getCurrentLocale());

## Playing Dialogs

The `SchnackFile` class has a field called `dialogs`, which is a map from `std::string` to
`std::shared_ptr<Dialog>`. This means that you can access dialogs by their name (the name
you chose in the tree view of the dialog editor):

    auto currentDialog = schnackFile->dialogs[dialogName];

When you want to start the playback of the particular dialog, you first have to get its entry node:

    auto currentNode = currentDialog->getEntryNode();

The `Node` class has a `step(dialog)` method, which can be called in order to evaluate logic of the
node. It will automatically check preconditions of the node's successors and even execute the
successors, if they are anything but *text nodes* or *question nodes*. This is due to the fact
that text nodes and question nodes require your assistance to be executed properly.

Whenever the execution of a chain of nodes reaches a text node, it will return a `TextStepResult`,
which contains the localized text that should be displayed to the user, the ID of the node that
contains this text, a reference to the character who says this text and a reference to the new
*current node*. In this case, you should probably display the `text` and the name of the `character`
on screen (since *Schnacker* doesn't know your engine, you have to do this). If you have voice acting
and followed the proposed naming convention for speech audio files, you can also play the audio
file with the ID of the node (again, you have to do this, because *Schnacker* is engine-agnostic).

In order to continue the dialog (after the audio playback is finished, or the player clicked *"next"*,
or however your game solves this), you can use the `currentNode` field of the `TextStepResult` and
call its `step(dialog)` method, which will then again execute all nodes from the current position until
it reaches a text node or a question node.

When a question node is reached, an `AnswersStepResult` is returned. In this case you should prompt
the player with the possible answers. These can be found in the `answers` field of the class.
It is a list (or rather `std::vector`) of tuples, containing a node ID and a localized string.
You can show the strings to the player. When the player chooses one of the options, you can call
the `chooseAnswer(dialog, nodeId)` method of the `AnswersStepResult`. It will then return a new
current node, which then again has a `step(dialog)` method to be called.

If the result is a null pointer or if the result is a `TextStepResult` containing a null pointer
for `currentNode`, the dialog's execution is done.


In simple C++ pseudo-code this would look approximately like this:

    // load the dialog
    auto currentDialog = schnackFile->dialogs[dialogName];
    // get the entry point
    auto currentNode = currentDialog->getEntryNode();

    // run until the dialog is done
    while(currentNode != nullptr)
    {
        // run the current node
        auto result = currentNode.step(currentDialog);

        // if there is no further node to execute, the dialog is finished
        if(result == nullptr)
            return;

        // the new current node is taken from the step result
        currentNode = result->currentNode;

        // if the result is a text result, show the text and play the voice
        auto textResult = std::dynamic_pointer_cast<TextStepResult>(result);
        if(textResult)
        {
            showText(textResult.character, textResult.text);
            playVoiceSample(textResult.nodeId);
        }
        else
        {
            // if the result is a question to the player, provide the possible answers
            // and wait for a choice. The new current node will depend on that choice
            auto answersResult = std::dynamic_pointer_cast<AnswersStepResult>(result);
            if(answersResult)
            {
                NodeId answerId = promptPlayerForAnswer(answersResult.answers);
                currentNode = answersResult->chooseAnswer(currentDialog, answerId);
            }
        }
    }

In a real-time game, this loop might not be fully sufficient, since you can not
stop the execution of the whole game until the player made a choice.
Further, this code doesn't make any break between the execution of single text nodes.
This means that there is no time for the player to read or even to listen to the
played voice file.

However, this is the most compact version that helps to understand how the library
is intended to be used.
