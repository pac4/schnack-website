---
title: 04 - Localization
userguide_author: Carsten Pfeffer
userguide_summary: This article describes how dialogs can be provided in different languages
_template: templates/_user-guide-article.html
---

If you followed the previous tutorials, you should have written a couple of texts
that will appear in the game. In this tutorial, we will translate them into another language,
so that people can experience the game in their native tongue.
If you followed the previous tutorials, you also might have closed the log and the localization
panel, since they were not important and took too much screen space. You can show the localization
panel by selecting *"View"* &rarr; *"Show Panel"* &rarr; *"Show Localization"* in the menu bar.

![Showing the localization panel]({{base_url}}/img/user-guide/04-show-localization-panel.png)

The localization panel will then float in the middle of the editor window. You can dock it
somewhere by clicking on the title bar of the panel and by dragging it to the border of another
docked panel. It should contain the texts of all text nodes and answer nodes (i.e., all texts
that will appear in the game). The first column is the ID of the corresponding node.
All further columns represent a particular locale. Currently, there is only `en` / English.

![The localization panel with one language]({{base_url}}/img/user-guide/04-localization-panel.png)

Right-click the column header and select *"Add new locale"* from the context menu in order to
create a new locale. Then, enter a name for the locale. In this example, we will add a German
translation (`de`).

![Adding a new locale]({{base_url}}/img/user-guide/04-add-locale.png)

A new column for the added locale will appear next to the `en` column. Now you can start to fill in
your translations for each node by double-clicking the table cell you want to edit.
Please don't be irritated if you don't speak German, just translate your texts ot another language
of your choice. If you find it hard to remember how the context of the text in the original text
column was, you can right-click the row and select the option *"Go to node"* in the context menu.
This will show the corresponding node in the node editor and you will be able to see the context
of the text lines:

![Go to node]({{base_url}}/img/user-guide/04-go-to-node.png)

If you want to see the new locale in the node editor, you can right-click the column header
of your locale and select *"Set 'de' as current locale"* (if `de` is your added locale).
This way you can switch the locale that is currently used by the editor.

Since localization is often not done by the game developers themselves, there is the possibility
to export the localization table to CSV. This allows you to hand the CSV file to a translator,
who can edit the CSV file in *Excel*, *Libre Office* or similar.
Then, you can re-import the localized CSV file into your *Schnack* file.

![Exporting a CSV file]({{base_url}}/img/user-guide/04-export-csv.png)

In *Libre Office Calc*, it now looks like the following:

![Exporting a CSV file]({{base_url}}/img/user-guide/04-libre-office.png)

It is important that the first two columns are not altered. They contain the node ID and
the speaking characters (since in some languages the particular text depends on who speaks,
male or female, etc. and this might be an important information to the translator).
You can then fill in the translations in the CSV file. Once done, you can import
the edited file via *"File"* &rarr; *"Import Localization From CSV"*
Note that the import will only add / override translated values, but never delete
existing values if they don't exist in the CSV. That way, you could provide two
CSV files, one to a Spanish translator and one to a Korean one and you could import
both one after another and then have both languages in your dialogs.

![The dialogs are translated]({{base_url}}/img/user-guide/04-translated-dialogs.png)

The picture above shows the fully translated version of Minerva's little great adventure.
The *Schnacker* runtimes allow you to select the locale at any time and will then
play the dialogs in the language of your choice.