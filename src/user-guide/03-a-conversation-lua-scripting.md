---
title: 03 - A Conversation / Lua Scripting
userguide_author: Carsten Pfeffer
userguide_summary: In this article, more complex nodes are used and Lua is utilized in order to introduce some logic
_template: templates/_user-guide-article.html
---

In the previous tutorial, we created a very simple dialog. In this tutorial, we will create a more advanced dialog
and utilize more logic nodes, as well as Lua scripting. An overview of the existing node types is available
[in the manual]({{base_url}}/manual/02-the-editor).

## A Second Character

The current version of our *Schnack* file only contains one character (our player). In order to create a conversation,
we need to create another one. Let's assume, we want our heroine to meet an NPC who complains about a plague of
rats in his basement. So let's add a new character with the canonical name `jack` and the display name `Jack the Miller`.
The canonical name will be used to refer to this character in the *Lua* scripts later on. Your characters panel should
now look similar to this:

![Two characters in the characters panel]({{base_url}}/img/user-guide/03-characters.png)

## Adding a Second Dialog

Now we will add another dialog by right-clicking the root element in the dialog tree and selecting the context menu entry
*"Add Dialog"* and calling it `talkToJack`. Select the new entry and add a new text node to the node editor in the middle
of the screen. If you have Jack selected in the characters panel, you will notice that the new text node will automatically
have him selected as the talking character. Let's add some text to the node which describes Jack's problem:
*"I believe the gods must hate me! I worked so hard to grind all this flour, but the rats in the basement will eat all of my hard work!"*

![The first text node]({{base_url}}/img/user-guide/03-first-text-node.png)

Now we will give our player the possibility to offer help with these rats in the basement. We will add a *question* node and two answer nodes.
Then, we will connect the two answer nodes to the question node and the question node to the initial text node. In the text for the
answer nodes we will write *"[offer help]"* and *"[do not offer help]"*:

![A question node]({{base_url}}/img/user-guide/03-a-question-node.png)

The entry point of this dialog will be the exposition of the NPC Jack.
Then, the question node is used to prompt a question to the player.
There are two possible answers: Either to offer help, or to not offer
help. Both options will be shown to the player. It is important to
understand that the text inside the answer nodes is only prompted
to the player and does not stand for spoken text. If we wanted our
player to say something when an answer is chosen, we have to add an
additional text node (remember: every spoken text is located inside
a text node):

![Text nodes follow the answer nodes]({{base_url}}/img/user-guide/03-answers-and-text.png)

If you accidently selected the wrong character in the characters panel
before adding the text nodes, you can change the speaking character of
a text node by clicking the circular avatar symbol.

You can now save the file and test it in the
[online dialog player]({{base_url}}/interpreter)
by dragging the file onto the website. You should now see two dialogs
in the list on the left-hand side. Select the newly created dialog and
press the start button.

![The second dialog in the online player]({{base_url}}/img/user-guide/03-first-test.png)

Jack will say his expository lines and then you will be prompted to
either offer help or to not offer help. Minerva, the player's avatar
will then respond accordingly.

## Choices Should Have Consequences

Now we have given the player a choice, but this choice is rather
meaningless, if there are no consequences for that choice.
Further, if we talk to Jack again, we will be prompted to make that
choice again, which doesn't seem like very satisfying player experience.
So let's modify Jack's dialog a little bit in order to make this 
decision more meaningful.

First, we insert a *branch* node before the first text node, make it
the entry node by right-clicking it and selecting *"Mark this node as entry"*
and connect its first output to the input of the original text node.

A branch will always continue with the first possible
successor node when executed (Think of it as a kind of *switch-case-statement*
if you are familiar with programming).

Check the *"Only once"* option for the original text node.
This will cause the node to be only executed once and not in further
executions of the dialog. Then, add two new text nodes and connect them to
the outputs of the new branch node. Add a text for the positive case to one
of the text nodes and a text for the negative case to the other one:

![Inserting a branch node as the new entry]({{base_url}}/img/user-guide/03-branch-node.png)

If you would execute the above dialog, Jack would describe his problem in the first iteration,
and the player would be able to make a choice. In the second iteration, Jack would always thank
the player for offering help, regardless of the choice that was made. This is due to the fact
that the node for the positive case is always executable and that we don't remember the choice
that was made.

In order to remember the choice, we need to introduce a new variable. We could introduce a
new global variable, but if we plan to add more NPCs, it is better to create a new property for
the character Jack. We do this by clicking the *"Add"* button in the properties section of the
characters panel (with Jack being the selected character in the list, otherwise we would add
the property to our player). The property could be called `offeredHelp` with the default value
`false`:

![A new property for Jack]({{base_url}}/img/user-guide/03-a-property.png)

Now we can use this property inside the *script* of our answer node.
Double-click the empty space below the *"Script"* label in the positive answer node,
write `characters.jack.offeredHelp = true` into the text box and confirm.
If provided, a script contains *Lua* code is always executed during the execution of
the node itself. This means that we now change the property value if the player chooses
to offer help. We can now use the same property in the *precondition* of the positive
case text node that follows the branch node which is the entry of the dialog.
A precondition, if provided, is a *Lua* script that returns a boolean value
(either `true` or `false`). If a precondition is provided, the corresponding node can
only be executed if its precondition evaluates to `true`.

Double-click the blank space next to the *"Precondition"* label of the positive-case
text node and enter the following code: `return characters.jack.offeredHelp`.
Now the first time, the player talks to Jack, he will explain his rat problem and the
player may choose to help or not. The decision is then stored in the property `offeredHelp`
of our character Jack (which is accessible through the lua expression `characters.jack.offeredHelp`).

When the dialog is executed further times, the *only once* flag will prevent Jack
from explaining his problem over and over again. Instead, the branch node will detect that
its first successor has already been executed and evaluate whether the second successor
can be executed (using the precondition) or whether the third successor has to be executed
(if the second one is not applicable due to its precondition).

![Using a script and a precondition]({{base_url}}/img/user-guide/03-using-script-and-precondition.png)

This would be a good point to save and test your dialog again. You will see that the second time
you start the dialog, Jack will have remembered your decision and react accordingly.

## A Bit of Variety

When you play that dialog over and over again, you will notice that it feels a little bit
mechanic when Jack repeats the same lines all the time. In order to improve the player experience,
we could add some variety by defining alternative versions of the negative case.

In order to do this, add a *random* node between the entry node (the branch node) and the text node
for the negative case. Then add two more text nodes with alternative texts where Jack complains about
the rats and connect them to the random node as well. The random node will select one of its successors
randomy when executed. This way, the character does not always say the same lines:

![Using a random node to add variety]({{base_url}}/img/user-guide/03-random-node.png)

Since this only works for the negative case, you can also do the same for the positive case.
However, remeber that you will have to move the precondition `return characters.jack.offeredHelp`
from the positive text node to the new random node for the positive case:

![Adding a random node for the positive case]({{base_url}}/img/user-guide/03-random-for-positive-case.png)

This should be a pretty complete example of a small conversation implemented in *Schnack*.
You could add some more text (for example making Jack react to Minerva's *"Ughh, this is disgusting..."* text),
but you should have gotten the idea how you create a dialog with *Schnack*.
There are two other node types we didn't use. First, the note / comment node, which is only for documentation.
You could add a note next to Jack's first text node and write there that this is the expository text, describing the problem.
This will make it easier for other people (or the future you) to understand how this dialog works.
The other node we didn't use is the empty node. It does pretty much nothing, but it has an *only once* flag,
a precondition and a script that could be utilized if some logic should be executed without actually
doing something else (displaying text, prompting the player, etc.). This idea is (like the splashscreen) borrowed
from *Blender 3D* where you can have empty objects in your scene which can only hold meta information and logic,
but are not actually renderable.

If you want to learn how to localize your dialogs, please follow the next tutorial.