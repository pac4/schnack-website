---
title: 03 - The Schnack File Format
manual_author: Carsten Pfeffer
manual_summary: A description of the JSON-based file format
_template: templates/_manual-article.html
---

The Schnack file format is based on the JSON standard (<a href="https://www.json.org" target="_blank">www.json.org</a>). Therefore, Schnack files can have either the file extension `.schnack` or `.json`. The root object of a Schnack file has the following properties:

- `name`: The name of the project (shown in the root element of the editor, mainly for informational purpose).
- `schnackVersion`: The version of the *Schnack* editor that was used to create the file
- `maxNodeId`: The highest node ID that was used for any node. This is used for determining the IDs of new nodes.
- `dialogs`: A list of different dialog objects representing conversations in a game.
- `emptyFolders`: A list of strings (the names of possibly existing
  folders that don't contain any dialogs). This is due to the fact that
  folder names are encoded in the dialog names, but folders without 
  dialogs would vanish after reloading the file.
- `characters`: The description of the characters who take part in the conversations.
- `variables`: The list of global variables used in the dialogs and their default values.
- `editorData`: This field contains information about the editor layout; the docked panels
  and the floating ones.
- `locales`: The list of existing locales.
- `localization`: A table of all texts that exist in the file. If there is only one locale,
  every table entry has only one string.

## Dialogs
A dialog is a sequence (or rather a graph) of text and logic nodes that
can be interpreted by a game. A dialog has the following properties:

- `name`: The name of the dialog. A dialog can be found by using this name.
          This means that the dialog name should be unique across the file.
- `viewport`: The position of the camera and the zoom level of the editor window
              at the time when saving the file.
- `entryNode`: Each dialog needs a starting point. The entryNode property
               contains the ID of the node that is interpreted first
               when running the dialog.
- `nodes`: The list of nodes is basically the content of the node editor,
           the essential dialog.
           Each node has at least the following properties:

    - `id`: An alphanumeric identifier with 10 characters. The ID is used to refer to that particular node.
    - `pos`: The (x, y) posision of the node in the editor (not needed
             for running the dialog in a game).
    - `onlyOnce`: If set to true, the node is only executed once and will
                  be skipped after it once has been played.
    - `next`: A list of node IDs that follow the current node.
              Depending on the type of the current node, there may be
              multiple or only one successor nodes.
    - `precondition` (optional): Each node may have a precondition that is
                                 evaluated before the node is considered for execution.
                                 When a precondition fails, the path introduced by the node can not be taken. The default scripting language for the
                                 precondition is *Lua* ([www.lua.org](https://www.lua.org/)).
    - `script` (optional): Similar to the precondition that is evaluated prior to executing
                           a node, there is a script property that can be used for
                           manipulating the global variables or character's properties.
    - `type`: Just like in the editor, the Schnack file has the following
              node types:
        - `text` nodes are used when a character says something. A text node
                 has the following additional properties:
            - `character`: The canonical name of the character who speaks the text
            - The actually spoken text is stored in the localization table.
        - `question`: Question nodes don't have any text (when a character asks the
                      question, this has to be modeled using an additional text node).
                      Next nodes for question nodes can only be answer nodes.
        - `answer`: Answer nodes are possible choices for the player to react on a question.
                    The text that is prompted to the user is stored in the localization table.
        - `random`: A random node chooses one of its next nodes randomly. All next nodes
                    that have a precondition that evaluates to `true` are considered.
        - `branch`: A branch node chooses the first of its next nodes that has a
                    precondition that evaluates to `true`.
        - `cycle`: A cycle node chooses one of its successor nodes. The successor node is
                   chosen in a round-robin manner. All successors with a precondition that
                   evaluates to `true` are considered.
        - `reference`: A reference node jumps to another dialog and starts
                       its execution with the respective entry node.
        - `empty`: An empty node does nothing, but can be used for executing scripts
                   on certain occasions.
        - `note`: A note or comment node is only for documentation purposes.
                  It is not considered when running the dialog, but rather used for
                  showing the user of the editor what the author tried to achieve.

## Characters
This list contains all characters who participate in the dialog.
Every spoken text line is associated with a character. A character has
the following properties:

- `canonicalName`: The canonical name is a unique name that identifies
                   the particular character within the dialog system.
                   This name will also be the name of the *Lua* variable
                   that refers to the properties of the character.
- `displayName`: The name as displayed in the game.
- `properties`: A character can have properties that may be used inside
                the *Lua* scripting logic of the dialogs.
                The properties of a character work like the global variables,
                but are only accessible via the character name and the
                property name (`characterName["propertyName"]` or
                `characterName.propertyName`).
                This means that each character has an own namespace for properties.

### Example

    "characters": [
        {
            "displayName": "Robby the robot",
            "canonicalName": "robot",
            "properties": {
                "batteryLevel": 0
            }
        },
        {
            "displayName": "Jane Doe",
            "canonicalName": "jane",
            "properties": {}
        }
    ]

## Variables
The variables section is a set of key-value-pairs where the key is the
name of the particular variable and the value is the initial value of
that variable.

The variables listed in this section will be available in the global *Lua*
namespace during the execution of the dialogs.

### Example

    "variables": {
        "shipHullIntegrity": 100,
        "playerHasAccessCard": false
    }

## Full Example

    {
        "name": "Minerva's Great Adventure",
        "schnackVersion": "0.3.0",
        "dialogs": [
            {
                "name": "book",
                "viewport": {
                    "x": 0,
                    "y": 0,
                    "zoom": 1
                },
                "entryNode": 0,
                "nodes": [
                    {
                        "id": "OO7belGIgw",
                        "type": "text",
                        "pos": {
                            "x": 116,
                            "y": 98.6875
                        },
                        "onlyOnce": false,
                        "next": [
                            "TEqagsdJDU"
                        ],
                        "character": "player"
                    },
                    {
                        "id": "TEqagsdJDU",
                        "type": "text",
                        "pos": {
                            "x": 451,
                            "y": 144.6875
                        },
                        "onlyOnce": false,
                        "next": [],
                        "character": "player"
                    }
                ]
            }
        ],
        "emptyFolders": [],
        "characters": [
            {
                "displayName": "Minerva the Sorceress",
                "canonicalName": "player",
                "properties": {}
            }
        ],
        "variables": {},
        "editorData": {
            "dockLayout": {
                "orientation": 1,
                "dockPercentage": -1,
                "children": [
                    {
                        "orientation": 2,
                        "dockPercentage": 0.20555555555555566,
                        "children": [
                            {
                                "dockPercentage": 0.5,
                                "identifier": "dialogTreePanel"
                            },
                            {
                                "dockPercentage": 0.5,
                                "identifier": "variablesPanel"
                            }
                        ]
                    },
                    {
                        "orientation": 2,
                        "dockPercentage": 0.6243383356070938,
                        "children": [
                            {
                                "dockPercentage": 0.6,
                                "identifier": "editorPanel"
                            },
                            {
                                "dockPercentage": 0.1,
                                "identifier": "logPanel"
                            },
                            {
                                "dockPercentage": 0.4,
                                "identifier": "localizationPanel"
                            }
                        ]
                    },
                    {
                        "dockPercentage": 0.17,
                        "identifier": "charactersPanel"
                    }
                ]
            },
            "floatingPanels": []
        },
        "locales": [
            "en"
        ],
        "localization": {
            "OO7belGIgw": [
                "The book cover looks like\na four-year old drew this."
            ],
            "TEqagsdJDU": [
                "I don't think I'll need this."
            ]
        }
    }