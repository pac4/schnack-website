---
title: 01 - Introduction
manual_author: Carsten Pfeffer
manual_summary: A short introduction
_template: templates/_manual-article.html
---

*"Schnack"* is an editor for dialogs that can be used in games.
*"Schnack"* is a north-german dialect word that essentially means *"chitchat"* or *"babble"*.
It is pronounced *"shnuck"*.

*Schnack* allows you to manage dialog text snippets and logic using a node-based editor
and the *Lua* scripting language ([www.lua.org](https://www.lua.org/)).
This means that characters and their properties (local variables), as well as global 
variables and their default values can be managed.