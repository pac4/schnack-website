---
title: 02 - The Editor
manual_author: Carsten Pfeffer
manual_summary: A description of all components of the editor
_template: templates/_manual-article.html
---

The editor consists of multiple panels that allow you to do different things.
The default layout of the panels is designed to provide the best tradeoff between
focus and overview. However, you are free to rearrange the panels so that they
fit your needs.

![Screenshot of the editor]({{base_url}}/img/manual/editor.png "The Editor")

## Dialogs Panel
Your game will most likely consist of more than one dialog. A dialog is a conversation
that can be started by an in-game event (e.g. the player clicks on an NPC). It is possible
that different characters take part in a dialog, but it is also absolutely reasonable
that only one single character has some text in a dialog (so in fact a dialog can also
be a monolog).

![Screenshot of the dialogs panel]({{base_url}}/img/manual/dialogsPanel.png "The Dialogs Panel")

### Creating a new Dialog
To create a new dialog, you can right-click in the root element in the dialogs panel.
Select the option `Add Dialog` in the context menu. A new child element will be added
to the root and you can now type in a name for the new dialog. Confirm the name by
pressing `Enter`. The name you provide here will be used in the game to refer to this
particular dialog. Left-click on the new dialog in order to start editing in the node
editor.

### Renaming a dialog
A dialog can be renamed by right-clicking the desired entry in the dialogs panel
and selecting `Rename Dialog` in the context menu. Then you can type the new name for
the dialog. By pressing `Enter`, the new name is confirmed. Pressing `Escape` will
cancel editing and the old name will be retained.

### Deleting a dialog
Deleting a dialog can be done by right-clicking the desired entry in the dialogs panel
and selecting `Delete Dialog` in the context menu. You will be prompted to confirm the
deletion of the dialog.

## Global Variables Panel

Variables can be used to describe the state of the game world. Global variables are
meant for information that is not specific to any character (there are character
properties for that). A variable entry in this panel consists of a unique name
and an initial value that is assigned at the start of the game, but may be altered
while running the dialogs.

![Screenshot of the global variables panel]({{base_url}}/img/manual/globalVariablesPanel.png "The Global Variables Panel")

### Creating a new variable
A new variable can be added by either right-clicking in the global variables panel
and selecting the option `add` or by pressing the `Add` buttom at the bottom of the panel.
You will then be prompted to enter a name for the new variable. Notice that this name
has to be a valid *Lua* identifier and that this identifier has to be unique across
the global variables and the canonical character names. The new variable will then
appear in the table of variables.

### Changing an existing variable
The name and the default value of a variable can be altered by double-clicking the
first column for the name or the second column for the default value. The new value
can be confirmed by pressing `Enter`. Alternatively, editing can be canceled by pressing
`Escape`.

## Characters Panel
The characters panel is used to manage the characters that take part in the conversations.
The bottom of the panel contains a list of all existing characters. Each entry shows the
characters' canonical name and the display name. The canonical name is used for 
identifying the character and available as a global variable in *Lua* your code.

The top of the panel shows the details of the currently selected character. This includes
the canonical name and the display name (which can be changed by double-clicking the 
respective label), as well as the character's properties. There's also a small icon for
the character which is meant to make it easier to identify the character in the node 
editor.

![Screenshot of the characters panel]({{base_url}}/img/manual/charactersPanel.png "The Characters Panel")

### Adding a Character
A new character can be addded by clicking the `Add` button on the bottom of the characters
panel (not to be confused with the `Add` button in the character properties list).
You will be prompted to enter a canonical name for the new character. When confirmed,
the new character will be added to the list.

### Renaming a Character
A character can be renamed by double-clicking the respective label in the character 
details at the top of the characters panel. A small popup window will prompt you for the
new name. Note that canonical names must be unique and be valid *Lua* identifiers.
They must not interfere with global variable names.

### Removing a Character
A character can be removed by selecting it in the character lsit and either pressing
the `Remove` button at the bottom of the characters panel or by right-clicking the
entry that should be removed and then selecting the context menu entry `Remove`.
You will then be prompted to confirm the deletion.

### Character Properties
Just like global variables, there are local variables, or rather *properties* per
character. They can be used to describe the state of a certain character.
Basically all facts that are described in the section about global variables also
apply for character properties, with the only exception that they are only accessible
via the character's canonical name, combined with the property name.
This means that character property names only have to be unique for that particular
character.

## Node Editor
The node editor is the heart of *Schnack*. This is where you write your dialog snippets
and connect them in order to create conversations with player choices and game logic.

![Screenshot of the node editor]({{base_url}}/img/manual/nodeEditor.png "The Node Editor")

Navigating in the node editor can be done using the mouse and keyboard shortcuts:

- **Zooming** can be done by using the scroll wheel of the mouse
- **Panning** can be done by either dragging around with the middle mouse button /
              scroll whell pressed, or by dragging around in the editor while
              pressing the `Ctrl` key.
- **Selecting single nodes** can be done by clicking on the top bar of the node
- **Selecting multiple nodes** can be done by dragging in the editor, using the
              selection rectangle.
- **Dragging nodes** is possible by clicking the node's top bar and dragging it
              around. All selected nodes will be dragged.

### Nodes
Each dialog needs a starting point, so there is always an entry node that is interpreted
first, when the dialog is run. This entry node has a purple outline. The entry node can
be changed by right-clicking any node and selecting the option `Mark this node as entry`.

#### Lua
The game logic is implemented using the **Lua** scripting language. It can be used to
control the flow of a conversation.

##### Preconditions
Each node is allowed to have a precondition. If provided, this precondition is evaluated
before considering the particular node for execution. If the precondition evaluates to
`true`, the node may be considered. If it evaluates to `false`, it can't be considered.
Note that it is usually desired that the evaluation of preconditions is free of
side-effects, meaning that it does not alter the state of the game.

##### Scripts
The selection of a particular conversation path might trigger some actions or manipulate
the state of the game (i.e. the values of global variables or character properties).
This can be done by providing a `Script` to a node. If provided, the script will be
executed whenever the corresponding node is run.

#### Only Once Flag
The `Only Once` flag allows you to create nodes that are only executed the first time
they occur. For all future runs, the node will not be considered. This can be useful
to create conversation paths that only occur when two characters never met before.
The *Schnack* runtime will take care that the particular nodes will only be executed once
if this flag is set.

### Node Types

#### Text Nodes
Text nodes are the only nodes that produce text that is spoken by any character.
In addition to the standard node properties (like precondition, only-once-flag and
script), they have a text content (the text that is spoken by the particular character)
and a reference to the speaking character. The character can be changed by either
clicking the character icon or the name of the character.

#### Question Nodes
Question nodes indicate a player interaction. They have multiple outputs and
must only be followed by answer nodes. The answers will then (if their precondition
evaluates to `true`) be prompted to the user and the user will be able to select
one of the possible options.

#### Answer Nodes
Answer nodes must follow a question node. The text inside the answer node is not spoken,
but only prompted to the user. If a character should speak the answer, the use of an
additional text node is adviced.

#### Random Nodes
Random nodes have multiple outputs, just like question nodes, but they allow any node
except an answer node as a successor. A random node selects a random node out of its
successors, so that characters can respond differently when approached multiple times.

#### Branch Nodes
Branch nodes work similar to random nodes, with the single difference that it does
not select a random successor, but the first node with a valid precondition.
This makes the execution of a branch node deterministic.

#### Empty Nodes
Empty nodes do nothing, but to provide the possibility to test a precondition and
run a script without doing something else (such as showing a text).

#### Comment Nodes
Comment nodes (also called notes) are a way to document the intention of the author.
They are not used inside the game and only meant to be read by other people editing
the dialog. In a way, they are equivalent to comments in a programming language.

## Log Panel
The log panel is mainly used for error reporting. In the current alpha state
of the editor, this refers mainly to bugs that occur during development. However,
having an eye on the log output might also help you to debug your *Lua* dialog logic.

## PDF Export
If you want your game to have voice recordings, you can use the PDF exporter to generate
printable PDF files for your voice actors. The PDF export can be performed via the `File`
menu using the option `Export PDF`.
A small popup window will allow you to select the character voice lines you want to
export. This can be useful if you want to either generate the full collection of text 
lines, or only want to export a document for a single voice actor.

![Screenshot of the pdf export dialog]({{base_url}}/img/manual/pdfExport.png "The PDF Export Dialog")

The generated document contains the names of the characters as headlines and the
node ID of the corresponding text nodes next to the spoken text. This should help
to make the association between text and sound file easier (if the sound files
are stored using the node ID as a file name).

![Screenshot of an exported]({{base_url}}/img/manual/pdf.png "An exported PDF")